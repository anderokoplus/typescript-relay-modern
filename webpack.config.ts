import { transform } from './config/transform';
import * as webpack from 'webpack';

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const config: webpack.Configuration = {
    entry: "./src/index.tsx",
    output: {
        filename: "bundle.js",
        path: __dirname + "/dist"
    },
    devtool: "source-map",
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json"],
        alias: {
            generated: path.resolve(__dirname, 'generated'),
        },
    },
    devServer: {
        contentBase: path.resolve(__dirname, 'www'),
        historyApiFallback: true,
        filename: `/dist/build.js`
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Starter',
            template: __dirname + '/src/index.ejs',
            inject: true,
            hash: true
        }),
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
            },
            // {
            //     test: /\.tsx?$/,
            //     exclude: /node_modules/,
            //     loader: "babel-loader"
            // },
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                options: {
                    compilerOptions: {
                        target: 'ES5'
                    },
                    getCustomTransformers: () => ({
                        after: [],
                        before: [
                            transform,
                        ],
                    }),
                    transpileOnly: true,
                },
                exclude: /node_modules/,
            },
        ]
    },
};
export default config;

