# Watch
1. https://www.howtographql.com/react-relay/0-introduction/
2. https://www.learnrelay.org

# Todo
2. enhance react-router4 to allow query aggregation (https://facebook.github.io/relay/docs/en/query-renderer.html) found-relay
3. baseUrl passing in components is bad, find a relative path plugin for react
5. pull schema before running relay compiler
6. prefix mutation handlers with "on"-prefix
7. implement subscribe client dispose
9. cache https://github.com/robrichard/relay-query-lookup-renderer

##### Terminal 1
```
$ npm install get-graphql-schema --save
$ get-graphql-schema http://localhost:4000/graphql > schema.graphql
$ npm run relay
```

##### Terminal 2
```
$ npm run dev
```
