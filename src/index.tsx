import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { App } from './app';
import '../www/assets/application.css';

ReactDOM.render(
    <App currentUser={null}/>,
    document.getElementById('root')
);
