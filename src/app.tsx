import * as React from 'react';
import { HashRouter } from 'react-router-dom';
import { HeaderWithRouter } from './components/header';
import { Route, Switch } from 'react-router';
import { ScreenHome } from './screens/home';
import { ScreenCaseHandling } from './screens/case/ScreenCaseHandling';
import { ScreenLogin } from './screens/login';
import { getCurrentUserIdOrNull } from './constants';
import { ScreenUser } from './screens/user/ScreenUser';
import { ScreenUserCursor } from './screens/userCursor/ScreenUserCursor';
import { ScreenUserOffset } from './screens/userOffset/ScreenUserOffset';

export interface AppProps {
    // todo :: load from localStorage
    currentUser: any;
}

export class App extends React.Component<AppProps> {
    render () {
        const userId = getCurrentUserIdOrNull();
        return (
            <HashRouter>
                {userId
                    ? (
                        <div className="layout-general">
                            <HeaderWithRouter currentUser={this.props.currentUser}/>
                            <Switch>
                                <Route path="/" exact component={ScreenHome}/>
                                <Route path="/cases" component={ScreenCaseHandling}/>
                                <Route path="/users/simple-list" component={ScreenUser}/>
                                <Route path="/users/cursor-pagination" component={ScreenUserCursor}/>
                                <Route path="/users/offset-pagination" component={ScreenUserOffset}/>
                            </Switch>
                        </div>
                    )
                    : (
                        <div className="layout-login">
                            <Route path="/" component={ScreenLogin}/>
                        </div>
                    )
                }
            </HashRouter>
        );
    }
}
