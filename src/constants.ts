export const TEMPORARY_USER_ID = 'VXNlcjox';
export const CR_USER_ID = 'comroom:user:id';
export const CR_AUTH_TOKEN = 'comroom:auth:token';
export const GRAPHQL_SUBSCRIPTION_ENDPOINT = 'ws://localhost:4000/subscriptions';
export const GRAPHQL_ENDPOINT = 'http://localhost:4000/graphql';
export const ITEMS_PER_PAGE = 10;

// todo :: move to helpers or something like that
export const getCurrentUserId = (): string => {
    // todo :: remove this hardcoded thingy
    const userId = localStorage.getItem(CR_USER_ID);
    if (userId === null) {
        throw new Error('User is not defined');
    }
    return userId;
};

export const getCurrentUserIdOrNull = (): string|null => {
    return localStorage.getItem(CR_USER_ID);
};

export const getAuthToken = (): string|null => {
    return localStorage.getItem(CR_AUTH_TOKEN);
};

export const clearUserData = () => {
    localStorage.removeItem(CR_USER_ID);
    localStorage.removeItem(CR_AUTH_TOKEN);
};
export const saveUserData = (id: string, token: string) => {
    localStorage.setItem(CR_USER_ID, id);
    localStorage.setItem(CR_AUTH_TOKEN, token);
};

