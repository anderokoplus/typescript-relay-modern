import { graphql, requestSubscription } from 'react-relay';
import { environment } from '../environment';

const CaseUpdateSubscription = graphql`
    subscription CaseUpdatedSubscription {
        caseUpdatedSubscription {
            node {
                id
                assignee {
                    id
                    firstName
                    lastName
                    role
                    image
                }
            }
        }
    }
`;

export const caseUpdatedSubscription = (variables: any) => {
    const subscriptionConfig = {
        subscription: CaseUpdateSubscription,
        variables,
        // updater: (proxyStore: RecordSourceSelectorProxy) => {
        // },
        onError: (error: Error) => {
            console.log('An error occurred', error);
        }
    };
    return requestSubscription(environment, subscriptionConfig);
};
