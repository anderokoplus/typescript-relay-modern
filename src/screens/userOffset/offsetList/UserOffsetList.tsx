import * as React from 'react';
import Helmet from 'react-helmet';
import { Table, TableHead, TableHeadCell, TableRowHeader } from '../../../components/ui/Table';
import { Icon } from '../../../components/ui/Icon';
import { createFragmentContainer, graphql } from 'react-relay';
import { Disposable, Environment } from 'relay-runtime';
import { OffsetPager } from '../../../components/pager/offsetPager';
import { UserRowContainer } from '../../user/list/UserRow';

export interface PaginationInfo {
    offset: number;
    limit: number;
    total: number;
}

export interface UserOffsetListProps {
    relay?: {
        environment: Environment,
        refetch: () => void, // See #refetch section
    },
    viewer: {
        users: any,
        paginationInfo: PaginationInfo
    },
    onNextPageClick: () => void,
    onPreviousPageClick: () => void,
    onGoToPageClick: (pageNumber: number) => void,
}

export interface UserOffsetListState {
}

export class UserOffsetList extends React.Component<UserOffsetListProps, UserOffsetListState> {
    subscription: Disposable;

    componentWillUnmount () {
        if (this.subscription) {
            this.subscription.dispose();
        }
    }

    render () {
        const users = this.props.viewer.users.edges.map((node: any) => node.node);
        const pagination = this.props.viewer.users.paginationInfo;

        return (
            <div className="page-content">
                <Helmet>
                    <title>Cases : ComRoom</title>
                </Helmet>
                <div className="filter01">
                    <div className="group start">
                        <button className="active">All cases</button>
                        <button>Assigned to me</button>
                        <button>Assigned</button>
                        <button>Unassigned</button>
                    </div>
                    <div className="group start">
                        <button className="active">All statuses</button>
                        <button>Open</button>
                        <button>Pending</button>
                        <button>Resolved</button>
                    </div>
                </div>
                {
                    users.length ? (
                        <div>
                            <Table className="default">
                                <TableHead>
                                    <TableHeadCell priority={1} className="w1p">Id</TableHeadCell>
                                    <TableHeadCell priority={1} className="w1p">FirstName</TableHeadCell>
                                    <TableHeadCell priority={1} className="w1p">LastName</TableHeadCell>
                                    <TableHeadCell priority={1} className="w1p">Role</TableHeadCell>
                                    <TableHeadCell priority={1} className="w1p">Image</TableHeadCell>
                                </TableHead>
                                {users.length > 0 ? <TableRowHeader>{users.length} queued cases</TableRowHeader> : null}
                                {users.map((user: any, i: number) => <UserRowContainer key={i} user={user}/>)}
                            </Table>
                            <OffsetPager
                                onNextPageClick={this.props.onNextPageClick}
                                onPreviousPageClick={this.props.onPreviousPageClick}
                                onGoToPageClick={this.props.onGoToPageClick}
                                offset={pagination.offset}
                                limit={pagination.limit}
                                total={pagination.total}
                            />
                        </div>
                    ) : (
                        <div className="pt-non-ideal-state">
                            <p className="illustration"><Icon kind="notifications" wrapperClassName="icon"/></p>
                            <p className="heading">No cases at the moment</p>
                        </div>
                    )
                }
            </div>
        );
    }
}

export const UserOffsetListContainer = createFragmentContainer(UserOffsetList, graphql`
    fragment UserOffsetList_viewer on Viewer {
        users(offset: $offset, limit: $limit) {
            edges {
                node {
                    ...UserRow_user
                }
            }
            paginationInfo {
                offset,
                limit,
                total,
            }
        }
    }
`);

