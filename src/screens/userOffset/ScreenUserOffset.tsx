import * as React from 'react';
import { graphql, QueryRenderer } from 'react-relay';
import { environment } from '../../environment';
import { LoadingError } from '../../components/nonideal/loadingerror';
import { Loading } from '../../components/nonideal/loading';
import Helmet from 'react-helmet';
import { UserOffsetListContainer } from './offsetList/UserOffsetList';

export interface ScreenUserOffsetProps {
    history: any,
}

export interface ScreenUserOffsetState {
    offset: number;
    limit: number;
}

export class ScreenUserOffset extends React.Component<ScreenUserOffsetProps, ScreenUserOffsetState> {
    nextPage = () => {
        this.setState({
            ...this.state,
            offset: this.state.offset + this.state.limit,
        });
    };
    previousPage = () => {
        this.setState({
            ...this.state,
            offset: this.state.offset - this.state.limit,
        });
    };
    goToPage = (pageNr: number) => {
        this.setState({
            ...this.state,
            offset: this.state.limit * (pageNr - 1),
        });

    };

    constructor (props: ScreenUserOffsetProps) {
        super(props);
        this.state = {
            limit: 2,
            offset: 0,
        }
    }

    render () {
        return (
            <QueryRenderer
                environment={environment}
                variables={{
                    offset: this.state.offset, // todo :: const
                    limit: this.state.limit,
                }}
                query={ScreenUserOffsetQuery}
                render={({ error, props }) => {
                    if (error) {
                        return (
                            <div className="layout-content">
                                <Helmet>
                                    <title>Users loading failed : ComRoom</title>
                                </Helmet>
                                <div className="page-content">
                                    <LoadingError message={error.message}/>
                                </div>
                            </div>
                        );
                    }
                    if (props) {
                        return (
                            <div className="layout-content">
                                <UserOffsetListContainer
                                    viewer={props.viewer}
                                    onNextPageClick={this.nextPage}
                                    onPreviousPageClick={this.previousPage}
                                    onGoToPageClick={this.goToPage}
                                />
                            </div>
                        );
                    }
                    return (
                        <div className="layout-content">
                            <Helmet>
                                <title>Loading Users : ComRoom</title>
                            </Helmet>
                            <div className="page-content">
                                <Loading/>
                            </div>
                        </div>
                    );
                }}
            />
        );
    }
}

const ScreenUserOffsetQuery = graphql`
    query ScreenUserOffsetListQuery(
    $offset: Int!,
    $limit: Int,
    ) {
        viewer {
            ...UserOffsetList_viewer
        }
    }
`;
