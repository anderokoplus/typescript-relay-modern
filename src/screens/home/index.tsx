import * as React from 'react';
import { Helmet } from 'react-helmet';
import { Icon } from '../../components/ui/Icon';

export interface ScreenHomeProps {}
export interface ScreenHomeState {}

export class ScreenHome extends React.Component<ScreenHomeProps, ScreenHomeState> {
    constructor(props: ScreenHomeProps) {
        super(props);
    }

    render() {
        return (
            <div className="layout-dashboard">
                <Helmet>
                    <title>Dashboard : ComRoom</title>
                </Helmet>
                <div className="page-content">
                    <div className="page-content-row">
                        <div className="block">
                            <Icon kind="user" wrapperClassName="bigicon"/>
                            <ul className="stats03">
                                <li>
                                    <div className="number">1231 <Icon kind="arrow2-up" wrapperClassName="icon positive"/></div>
                                    <div className="label">Clients on site</div>
                                </li>
                                <li>
                                    <div className="number">123123 <Icon kind="arrow2-down" wrapperClassName="icon negative"/></div>
                                    <div className="label">Visits in 24h</div>
                                </li>
                                <li>
                                    <div className="number">234 <Icon kind="arrow2-up" wrapperClassName="icon"/></div>
                                    <div className="label">Cases opened</div>
                                </li>
                                <li>
                                    <div className="number">221 <Icon kind="blank" wrapperClassName="icon"/></div>
                                    <div className="label">Cases closed</div>
                                </li>
                                <li>
                                    <div className="number">1231 <Icon kind="arrow2-up" wrapperClassName="icon positive"/></div>
                                    <div className="label">Events triggered</div>
                                </li>
                            </ul>
                        </div>
                        <div className="block">
                            <Icon kind="timer" wrapperClassName="bigicon"/>
                            <ul className="stats03">
                                <li>
                                    <div className="number">12/40 <Icon kind="arrow2-up" wrapperClassName="icon positive"/></div>
                                    <div className="label">Ongoing chats</div>
                                </li>
                                <li>
                                    <div className="number">6.2 <Icon kind="arrow2-up" wrapperClassName="icon negative"/></div>
                                    <div className="label">Queue length</div>
                                </li>
                                <li>
                                    <div className="number">2m 12 <Icon kind="arrow2-up" wrapperClassName="icon negative"/></div>
                                    <div className="label">Wait time</div>
                                </li>
                                <li>
                                    <div className="number">12m 12s <Icon kind="arrow2-up" wrapperClassName="icon negative"/></div>
                                    <div className="label">Resolve time</div>
                                </li>
                                <li>
                                    <div className="number">33 <Icon kind="arrow2-up" wrapperClassName="icon negative"/></div>
                                    <div className="label">Left while waiting</div>
                                </li>
                            </ul>
                        </div>
                        <div className="block">
                            <Icon kind="chat" wrapperClassName="bigicon"/>
                            <ul className="stats03">
                                <li>
                                    <div className="number">5m 12s <Icon kind="arrow2-up" wrapperClassName="icon negative"/></div>
                                    <div className="label">Chat duration</div>
                                </li>
                                <li>
                                    <div className="number">10s <Icon kind="arrow2-up" wrapperClassName="icon positive"/></div>
                                    <div className="label">First response</div>
                                </li>
                                <li>
                                    <div className="number">65 <Icon kind="arrow2-up" wrapperClassName="icon"/></div>
                                    <div className="label">Messages per chat</div>
                                </li>
                                <li>
                                    <div className="number">343 <Icon kind="arrow2-up" wrapperClassName="icon positive"/></div>
                                    <div className="label">Sceens shared</div>
                                </li>
                                <li>
                                    <div className="number">4.5 <Icon kind="arrow2-up" wrapperClassName="icon positive"/></div>
                                    <div className="label">Rating</div>
                                </li>
                            </ul>
                        </div>
                        <div className="block">
                            <Icon kind="homepage" wrapperClassName="bigicon"/>
                            <ul className="stats03">
                                <li>
                                    <div className="number">1212 <Icon kind="arrow2-up" wrapperClassName="icon positive"/></div>
                                    <div className="label"><a href="#">Page title here</a></div>
                                </li>
                                <li>
                                    <div className="number">920 <Icon kind="arrow2-up" wrapperClassName="icon positive"/></div>
                                    <div className="label"><a href="#">Page title here</a></div>
                                </li>
                                <li>
                                    <div className="number">675 <Icon kind="arrow2-down" wrapperClassName="icon negative"/></div>
                                    <div className="label"><a href="#">Page title here</a></div>
                                </li>
                                <li>
                                    <div className="number">343 <Icon kind="arrow2-down" wrapperClassName="icon negative"/></div>
                                    <div className="label"><a href="#">Page title here</a></div>
                                </li>
                                <li>
                                    <div className="number">221 <Icon kind="arrow2-up" wrapperClassName="icon positive"/></div>
                                    <div className="label"><a href="#">Page title here</a></div>
                                </li>
                            </ul>
                        </div>
                        <div className="block">
                            <Icon kind="flag" wrapperClassName="bigicon"/>
                            <ul className="stats03">
                                <li>
                                    <div className="number">1212 <Icon kind="arrow2-up" wrapperClassName="icon positive"/></div>
                                    <div className="label"><a href="#">Trigger name</a></div>
                                </li>
                                <li>
                                    <div className="number">920 <Icon kind="arrow2-up" wrapperClassName="icon positive"/></div>
                                    <div className="label"><a href="#">Trigger name</a></div>
                                </li>
                                <li>
                                    <div className="number">675 <Icon kind="arrow2-up" wrapperClassName="icon positive"/></div>
                                    <div className="label"><a href="#">Trigger name</a></div>
                                </li>
                                <li>
                                    <div className="number">343 <Icon kind="arrow2-up" wrapperClassName="icon positive"/></div>
                                    <div className="label"><a href="#">Trigger name</a></div>
                                </li>
                                <li>
                                    <div className="number">221 <Icon kind="arrow2-up" wrapperClassName="icon positive"/></div>
                                    <div className="label"><a href="#">Trigger name</a></div>
                                </li>
                            </ul>
                        </div>
                        <div className="block">
                            <Icon kind="tag" wrapperClassName="bigicon"/>
                            <ul className="stats03">
                                <li>
                                    <div className="number">1212 <Icon kind="arrow2-up" wrapperClassName="icon positive"/></div>
                                    <div className="label"><a href="#">Tag name</a></div>
                                </li>
                                <li>
                                    <div className="number">920 <Icon kind="arrow2-up" wrapperClassName="icon positive"/></div>
                                    <div className="label"><a href="#">Tag name</a></div>
                                </li>
                                <li>
                                    <div className="number">675 <Icon kind="arrow2-up" wrapperClassName="icon positive"/></div>
                                    <div className="label"><a href="#">Tag name</a></div>
                                </li>
                                <li>
                                    <div className="number">343 <Icon kind="arrow2-up" wrapperClassName="icon positive"/></div>
                                    <div className="label"><a href="#">Tag name</a></div>
                                </li>
                                <li>
                                    <div className="number">221 <Icon kind="arrow2-down" wrapperClassName="icon negative"/></div>
                                    <div className="label"><a href="#">Tag name</a></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
