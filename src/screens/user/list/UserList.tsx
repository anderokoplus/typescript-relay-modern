import * as React from 'react';
import Helmet from 'react-helmet';
import { Table, TableHead, TableHeadCell, TableRowHeader } from '../../../components/ui/Table';
import { Icon } from '../../../components/ui/Icon';
import { createFragmentContainer, graphql } from 'react-relay';
import { Disposable } from 'relay-runtime';
import { UserRowContainer } from './UserRow';

export interface UserListProps {
    viewer: {
        users: any,
    }
}

export interface UserListState {
}

export class UserList extends React.Component<UserListProps, UserListState> {
    subscription: Disposable;

    constructor (props: UserListProps) {
        super(props);
    }

    componentWillUnmount () {
        if (this.subscription) {
            this.subscription.dispose();
        }
    }

    render () {
        const users = this.props.viewer.users.edges.map((node: any) => node.node);
        return (
            <div className="page-content">
                <Helmet>
                    <title>Cases : ComRoom</title>
                </Helmet>
                <div className="filter01">
                    <div className="group start">
                        <button className="active">All cases</button>
                        <button>Assigned to me</button>
                        <button>Assigned</button>
                        <button>Unassigned</button>
                    </div>
                    <div className="group start">
                        <button className="active">All statuses</button>
                        <button>Open</button>
                        <button>Pending</button>
                        <button>Resolved</button>
                    </div>
                </div>
                {
                    users.length ? (
                        <Table className="default">
                            <TableHead>
                                <TableHeadCell priority={1} className="w1p">Id</TableHeadCell>
                                <TableHeadCell priority={1} className="w1p">FirstName</TableHeadCell>
                                <TableHeadCell priority={1} className="w1p">LastName</TableHeadCell>
                                <TableHeadCell priority={1} className="w1p">Role</TableHeadCell>
                                <TableHeadCell priority={1} className="w1p">Image</TableHeadCell>
                            </TableHead>
                            {users.length > 0 ? <TableRowHeader>{users.length} queued cases</TableRowHeader> : null}
                            {users.map((user: any, i: number) => <UserRowContainer key={i} user={user}/>)}
                        </Table>
                    ) : (
                        <div className="pt-non-ideal-state">
                            <p className="illustration"><Icon kind="notifications" wrapperClassName="icon"/></p>
                            <p className="heading">No cases at the moment</p>
                        </div>
                    )
                }
            </div>
        );
    }
}

// todo :: add  @connection(key: "CaseList_data", filters: [])
export const UserListContainer = createFragmentContainer(UserList, graphql`
    fragment UserList_viewer on Viewer {
        users {
            edges {
                node {
                    ...UserRow_user
                }
            }
        }
    }
`);
