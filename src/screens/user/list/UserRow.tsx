import * as React from 'react';
import { TableRow, TableRowCell } from '../../../components/ui/Table';
import { createFragmentContainer, graphql } from 'react-relay';

export interface UserRowProps {
    user: any;
}

export interface UserRowState {
}

class UserRow extends React.Component<UserRowProps, UserRowState> {
    render () {
        const { user } = this.props;
        return (
            <TableRow>
                <TableRowCell className="nowrap">{user.id}</TableRowCell>
                <TableRowCell className="nowrap">{user.firstName}</TableRowCell>
                <TableRowCell className="nowrap">{user.lastName}</TableRowCell>
                <TableRowCell className="nowrap">{user.role}</TableRowCell>
                <TableRowCell className="nowrap">{user.image}</TableRowCell>
            </TableRow>
        );
    }
}

export const UserRowContainer = createFragmentContainer(UserRow, graphql`
    fragment UserRow_user on User {
        id
        firstName
        lastName
        role
        image
    }
`);
