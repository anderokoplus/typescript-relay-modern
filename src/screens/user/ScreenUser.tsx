import * as React from 'react';
import { graphql, QueryRenderer } from 'react-relay';
import { environment } from '../../environment';
import { LoadingError } from '../../components/nonideal/loadingerror';
import { Loading } from '../../components/nonideal/loading';
import Helmet from 'react-helmet';
import { UserListContainer } from './list/UserList';

export interface ScreenUserProps {
    history: any,
}

export interface ScreenUserState {
    users: any[];
}

export class ScreenUser extends React.Component<ScreenUserProps, ScreenUserState> {
    constructor (props: ScreenUserProps) {
        super(props);
    }

    render () {
        return (
            <QueryRenderer
                environment={environment}
                variables={{}}
                query={ScreenUserQuery}
                render={({ error, props }) => {
                    if (error) {
                        return (
                            <div className="layout-content">
                                <Helmet>
                                    <title>Users loading failed : ComRoom</title>
                                </Helmet>
                                <div className="page-content">
                                    <LoadingError message={error.message}/>
                                </div>
                            </div>
                        );
                    }
                    if (props) {
                        // todo :: refactor this and move stuff where it belongs
                        // const allCases = props.viewer.allCases.edges.map((node: any) => node.node);
                        return (
                            <div className="layout-content">
                                <UserListContainer viewer={props.viewer}/>
                            </div>
                        );
                    }
                    return (
                        <div className="layout-content">
                            <Helmet>
                                <title>Loading Users : ComRoom</title>
                            </Helmet>
                            <div className="page-content">
                                <Loading/>
                            </div>
                        </div>
                    );
                }}
            />
        );
    }
}

const ScreenUserQuery = graphql`
    query ScreenUserListQuery {
        viewer {
            ...UserList_viewer
        }
    }
`;
