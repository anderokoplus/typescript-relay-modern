import * as React from 'react';
import Helmet from 'react-helmet';
import { Icon } from '../../components/ui/Icon';
import { LoginFormWithRouter } from '../../components/loginForm';

export class ScreenLogin extends React.Component<{}, {}> {
    render() {
        return (
            <div className="layout-content">
                <Helmet>
                    <title>Log In : ComRoom</title>
                </Helmet>
                <div className="page-illustration">
                    <Icon kind="logo-vertical" wrapperClassName="logo" />
                </div>
                <div className="page-content">
                    <div className="page-content-inner">
                        <h1>Please login</h1>
                        <LoginFormWithRouter/>
                    </div>
                </div>
            </div>
        );
    }
}
