import * as React from 'react';
import { graphql, QueryRenderer } from 'react-relay';
import { environment } from '../../environment';
import { LoadingError } from '../../components/nonideal/loadingerror';
import { Loading } from '../../components/nonideal/loading';
import Helmet from 'react-helmet';
import { UserCursorListContainer } from './cursorList/UserCursorList';
import { ITEMS_PER_PAGE } from '../../constants';

export interface ScreenUserCursorProps {
    history: any,
}

export interface ScreenUserCursorState {
    users: any[];
}

export class ScreenUserCursor extends React.Component<ScreenUserCursorProps, ScreenUserCursorState> {
    constructor (props: ScreenUserCursorProps) {
        super(props);
    }

    componentDidMount () {
    }

    componentWillUnmount () {
    }

    render () {
        return (
            <QueryRenderer
                environment={environment}
                variables={{
                    first: ITEMS_PER_PAGE,
                }}
                query={ScreenUserCursorQuery}
                render={({ error, props }) => {
                    if (error) {
                        return (
                            <div className="layout-content">
                                <Helmet>
                                    <title>Users loading failed : ComRoom</title>
                                </Helmet>
                                <div className="page-content">
                                    <LoadingError message={error.message}/>
                                </div>
                            </div>
                        );
                    }
                    if (props) {
                        return (
                            <div className="layout-content">
                                <UserCursorListContainer viewer={props.viewer}/>
                            </div>
                        );
                    }
                    return (
                        <div className="layout-content">
                            <Helmet>
                                <title>Loading Users : ComRoom</title>
                            </Helmet>
                            <div className="page-content">
                                <Loading/>
                            </div>
                        </div>
                    );
                }}
            />
        );
    }
}

const ScreenUserCursorQuery = graphql`
    query ScreenUserCursorListQuery(
    $first: Int!,
    $after: String,
    ) {
        viewer {
            ...UserCursorList_viewer
        }
    }
`;
