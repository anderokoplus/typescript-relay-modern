import * as React from 'react';
import Helmet from 'react-helmet';
import { Table, TableHead, TableHeadCell, TableRowHeader } from '../../../components/ui/Table';
import { Icon } from '../../../components/ui/Icon';
import { createPaginationContainer, graphql } from 'react-relay';
import { Disposable } from 'relay-runtime';
import { ITEMS_PER_PAGE } from '../../../constants';
import { UserRowContainer } from '../../user/list/UserRow';

export interface UserCursorListProps {
    relay?: any;
    viewer: {
        users: any,
    }
}

export interface UserCursorListState {
}

export class UserCursorList extends React.Component<UserCursorListProps, UserCursorListState> {
    subscription: Disposable;
    loadMore = () => {
        if (!this.props.relay.hasMore()) {
            console.log('nothing more to load');
            return;
        } else if (this.props.relay.isLoading()) {
            console.log('request is already pending ');
            return;
        }
        this.props.relay.loadMore(ITEMS_PER_PAGE);
    };

    componentWillUnmount () {
        if (this.subscription) {
            this.subscription.dispose();
        }
    }

    render () {
        const users = this.props.viewer.users.edges.map((node: any) => node.node);
        return (
            <div className="page-content">
                <Helmet>
                    <title>Cases : ComRoom</title>
                </Helmet>
                <div className="filter01">
                    <div className="group start">
                        <button className="active">All cases</button>
                        <button>Assigned to me</button>
                        <button>Assigned</button>
                        <button>Unassigned</button>
                    </div>
                    <div className="group start">
                        <button className="active">All statuses</button>
                        <button>Open</button>
                        <button>Pending</button>
                        <button>Resolved</button>
                    </div>
                </div>
                {
                    users.length ? (
                        <Table className="default">
                            <TableHead>
                                <TableHeadCell priority={1} className="w1p">Id</TableHeadCell>
                                <TableHeadCell priority={1} className="w1p">FirstName</TableHeadCell>
                                <TableHeadCell priority={1} className="w1p">LastName</TableHeadCell>
                                <TableHeadCell priority={1} className="w1p">Role</TableHeadCell>
                                <TableHeadCell priority={1} className="w1p">Image</TableHeadCell>
                            </TableHead>
                            {users.length > 0 ? <TableRowHeader>{users.length} queued cases</TableRowHeader> : null}
                            {users.map((user: any, i: number) => <UserRowContainer key={i} user={user}/>)}
                        </Table>
                    ) : (
                        <div className="pt-non-ideal-state">
                            <p className="illustration"><Icon kind="notifications" wrapperClassName="icon"/></p>
                            <p className="heading">No cases at the moment</p>
                        </div>
                    )
                }
                <div>
                    <div onClick={() => {
                        this.loadMore();
                    }}>Load more
                    </div>
                </div>
            </div>
        );
    }
}

export const UserCursorListContainer = createPaginationContainer(
    UserCursorList,
    graphql`
        fragment UserCursorList_viewer on Viewer {
            users(first: $first, after: $after) @connection(key: "UserCursorList_users", filters: []) {
                edges {
                    node {
                        ...UserRow_user
                    }
                }
                pageInfo {
                    hasNextPage
                    endCursor
                }
            }
        },
    `,
    {
        direction: 'forward',
        query: graphql`
            query UserCursorListForwardQuery(
            $first: Int,
            $after: String,
            ) {
                viewer {
                    ...UserCursorList_viewer
                }
            }
        `,
        getConnectionFromProps (props) {
            return props.viewer && props.viewer.users;
        },
        getFragmentVariables (previousVariables, totalCount) {
            return {
                ...previousVariables,
                count: totalCount,
            };
        },
        getVariables (props, paginationInfo, fragmentVariables) {
            return {
                count: paginationInfo.count,
                after: paginationInfo.cursor,
            }
        }

    }
);
