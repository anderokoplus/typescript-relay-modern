import * as React from 'react';
import Helmet from 'react-helmet';
import { CaseRowContainer } from './CaseRow';
import { Table, TableHead, TableHeadCell, TableRowHeader } from '../../../components/ui/Table';
import { Icon } from '../../../components/ui/Icon';
import { createFragmentContainer, graphql } from 'react-relay';
import { caseUpdatedSubscription } from '../../../subscriptions/CaseUpdatedSubscription';
import { Disposable } from 'relay-runtime';

export interface CaseListProps {
    baseUrl: string;
    caseActions: any;
    viewer: {
        queuedCases: any,
        assignedCases: any,
    }
}
export interface CaseListState {
    filterType?: string;
}

export class CaseList extends React.Component<CaseListProps, CaseListState> {
    subscription: Disposable;

    componentDidMount () {
        this.subscription = caseUpdatedSubscription({});
    }

    componentWillUnmount () {
        if (this.subscription) {
            this.subscription.dispose();
        }
    }


    constructor(props: CaseListProps) {
        super(props);
        this.state = {
            filterType: 'simple'
        };
    }

    render() {
        const queuedCases = this.props.viewer.queuedCases.edges.map((node: any) => node.node);
        const assignedCases = this.props.viewer.assignedCases.edges.map((node: any) => node.node);
        const caseCount = queuedCases.length + assignedCases.length;
        return (
            <div className="page-content">
                <Helmet>
                    <title>Cases : ComRoom</title>
                </Helmet>
                <div className="filter01">
                    { this.state.filterType === 'simple' ? (
                        <div className="group start">
                            <button className="active">All cases</button>
                            <button>Assigned to me</button>
                            <button>Assigned</button>
                            <button>Unassigned</button>
                        </div>
                    ) : null }
                    { this.state.filterType === 'simple' ? (
                        <div className="group start">
                            <button className="active">All statuses</button>
                            <button>Open</button>
                            <button>Pending</button>
                            <button>Resolved</button>
                        </div>
                    ) : null }
                    { this.state.filterType === 'advanced' ? (
                        <h1>Case handling</h1>
                    ) : null }
                    <div className="group end">
                        <button className={this.state.filterType === 'simple' ? 'active' : ''} onClick={() => { this.setState({ filterType: 'simple' }); }}>Simple filter</button>
                        <button className={this.state.filterType === 'advanced' ? 'actove' : ''} onClick={() => { this.setState({ filterType: 'advanced' }); }}>Advanced</button>
                    </div>
                </div>
                { this.state.filterType === 'advanced' ? (
                    <div className="panel">
                        <div className="form horizontal filter">
                            <div className="fields">
                                <div className="row">
                                    <div className="label">Agent</div>
                                    <div className="items">
                                        <input type="text" className="pt-form-control"/>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="label">Client</div>
                                    <div className="items">
                                        <input type="text" className="pt-form-control"/>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="label">Status</div>
                                    <div className="items">
                                        <input type="text" className="pt-form-control"/>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="label">Country</div>
                                    <div className="items">
                                        <input type="text" className="pt-form-control"/>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="label">Page</div>
                                    <div className="items">
                                        <input type="text" className="pt-form-control"/>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="label">Trigger</div>
                                    <div className="items">
                                        <input type="text" className="pt-form-control"/>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="label">Tags</div>
                                    <div className="items">
                                        <input type="text" className="pt-form-control"/>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="label">Text</div>
                                    <div className="items">
                                        <input type="text" className="pt-form-control"/>
                                    </div>
                                </div>
                                <div className="row empty"/>
                                <div className="row empty"/>
                                <div className="row empty"/>
                                <div className="row empty"/>
                            </div>
                            <ul className="buttons">
                                <li>
                                    <button className="pt-button pt-intent-primary">Apply</button>
                                </li>
                                <li>
                                    <button className="pt-button">Reset</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                ) : null }
                {
                    caseCount ? (
                        <Table className="default">
                            <TableHead>
                                <TableHeadCell priority={1} className="w1p">Created</TableHeadCell>
                                <TableHeadCell priority={5}>Updated</TableHeadCell>
                                <TableHeadCell priority={2}>Client</TableHeadCell>
                                <TableHeadCell className="center">Country</TableHeadCell>
                                <TableHeadCell priority={4}>Page</TableHeadCell>
                                <TableHeadCell priority={4}>Trigger</TableHeadCell>
                                <TableHeadCell>Last visit</TableHeadCell>
                                <TableHeadCell className="center">Visits</TableHeadCell>
                                <TableHeadCell priority={5} className="center">Open Cases</TableHeadCell>

                                <TableHeadCell>Agent</TableHeadCell>
                                <TableHeadCell>Status</TableHeadCell>
                                <TableHeadCell hidden={true}>Details</TableHeadCell>
                                <TableHeadCell priority={3} className="w1p"/>
                            </TableHead>
                            {queuedCases.length > 0 ? <TableRowHeader>{ queuedCases.length } queued cases</TableRowHeader> : null}
                            {queuedCases.map((item: any, i: number) => <CaseRowContainer key={i} item={item} baseUrl={this.props.baseUrl} caseActions={this.props.caseActions}/>)}
                            {assignedCases.length > 0 ? <TableRowHeader>{ assignedCases.length } assigned cases</TableRowHeader> : null}
                            {assignedCases.map((item: any, i: number) => <CaseRowContainer key={i} item={item} baseUrl={this.props.baseUrl} caseActions={this.props.caseActions}/>)}
                        </Table>
                    ) : (
                        <div className="pt-non-ideal-state">
                            <p className="illustration"><Icon kind="notifications" wrapperClassName="icon"/></p>
                            <p className="heading">No cases at the moment</p>
                        </div>
                    )
                }
            </div>
        );
    }
}
export const CaseListContainer = createFragmentContainer(CaseList, graphql`
    fragment CaseList_viewer on Viewer {
        queuedCases: cases(first: 10, isQueued: true) {
            edges {
                node {
                    ...CaseRow_item
                }
            }
        }
        assignedCases: cases(first: 10, isAssigned: true) {
            edges {
                node {
                    ...CaseRow_item
                }
            }
        }
    }
`);
