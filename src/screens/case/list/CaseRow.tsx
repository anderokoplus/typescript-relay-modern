import * as React from 'react';
import * as moment from 'moment';
import { Link } from 'react-router-dom';
import { TableRow, TableRowCell } from '../../../components/ui/Table';
import { createFragmentContainer, graphql } from 'react-relay';
import { assignCase } from '../../../mutations/AssignCaseMutation';
import { unAssignCase } from '../../../mutations/UnAssignCaseMutation';
import { getCurrentUserId, TEMPORARY_USER_ID } from '../../../constants';

export interface CaseRowProps {
    baseUrl: string;
    caseActions: any;
    item: any;
}

export interface CaseRowState {
}

class CaseRow extends React.Component<CaseRowProps, CaseRowState> {
    constructor (props: CaseRowProps) {
        super(props);
    }

    assignCase = (event: any, caseId: string, userId: string) => {
        event.preventDefault();
        assignCase(
            caseId,
            userId,
            (response) => { console.log(response); },
            (error) => { console.error(error); }
        );
    };

    unAssignCase = (event: any, caseId: string) => {
        event.preventDefault();
        unAssignCase(
            caseId,
            (response) => { console.log(response); },
            (error) => { console.error(error); }
        );
    };

    render () {
        const { item, caseActions, baseUrl } = this.props;
        return (
            <TableRow>
                <TableRowCell className="nowrap">{moment.unix(item.created_utc).fromNow()}</TableRowCell>
                <TableRowCell className="number">{moment.unix(item.last_update_utc).fromNow()}</TableRowCell>
                <TableRowCell>{item.authorized ? item.client.first_name + ' ' + item.client.last_name : 'Anonymous'}</TableRowCell>
                <TableRowCell className="center">
                    <img src={'assets/flags/' + item.session.country + '.svg'} className="flag" title=""/>
                </TableRowCell>
                <TableRowCell>{item.session.domain} » {item.session.current_page}</TableRowCell>
                <TableRowCell>Chat button</TableRowCell>
                <TableRowCell>3 days ago</TableRowCell>
                <TableRowCell className="center">{item.session.visits}</TableRowCell>
                <TableRowCell className="center">{item.authorized ? '4' : '-'}</TableRowCell>
                <TableRowCell>{item.assignee ? item.assignee.first_name + ' ' + item.assignee.last_name : '-'}</TableRowCell>
                <TableRowCell className="nowrap">{item.status}</TableRowCell>
                <TableRowCell className="support-table-details01">
                    <div>ID: {item.id}</div>
                    <div>Title: {item.title}</div>
                    <div>Tags: {item.tags.length}</div>
                    <div>Description: Some lorem ipsum</div>
                </TableRowCell>
                <TableRowCell>
                    {item.assignee ? (
                        item.assignee.id === TEMPORARY_USER_ID ? (
                            <ul className="buttons pt-small justify">
                                <li>
                                    <Link
                                        className="pt-button pt-intent-primary" to={baseUrl + '/' + item.id}
                                        onClick={() => {
                                              caseActions.observeCase(item.id);
                                        }}>
                                        Continue
                                    </Link>
                                </li>
                                <li>
                                    <a href="#" className="pt-button" onClick={(e) => { this.unAssignCase(e, item.id); }}>
                                        UnAssign
                                    </a>
                                </li>
                            </ul>
                        ) : (
                            <ul className="buttons pt-small justify">
                                <li>
                                    <Link
                                        className="pt-button"
                                        to={baseUrl + '/' + item.id}
                                        onClick={() => { caseActions.acceptCase(item.id); }}>
                                        Take over
                                    </Link>
                                </li>
                                <li>
                                    <Link
                                        className="pt-button"
                                        to={baseUrl + '/' + item.id}
                                        onClick={() => { caseActions.observeCase(item.id); }}>
                                        Observe
                                    </Link>
                                </li>
                                <li>
                                    <a href="#" className="pt-button" onClick={(e) => { this.assignCase(e, item.id, getCurrentUserId()); }}>
                                        Assign
                                    </a>
                                </li>
                            </ul>
                        )
                    ) : (
                        <ul className="buttons pt-small justify">
                            <li>
                                <Link
                                    className="pt-button pt-intent-primary"
                                    to={baseUrl + '/' + item.id}
                                    onClick={() => { caseActions.acceptCase(item.id); }}
                                >
                                    Accept
                                </Link>
                            </li>
                            <li>
                                <Link
                                    className="pt-button"
                                    to={baseUrl + '/' + item.id}
                                    onClick={() => { caseActions.observeCase(item.id); }}
                                >
                                    Observe
                                </Link>
                            </li>
                            <li>
                                <a href="#" className="pt-button" onClick={(e) => { this.assignCase(e, item.id, getCurrentUserId()); }} >
                                    Assign
                                </a>
                            </li>
                        </ul>
                    )}
                </TableRowCell>
            </TableRow>
        );
    }
}

export const CaseRowContainer = createFragmentContainer(CaseRow, graphql`
    fragment CaseRow_item on Case {
        id
        session {
            country
        }
        assignee {
            id
            firstName
            lastName
            role
            image
        }
        tags {
            id
            label
            type
        }
    }
`);
