import * as React from 'react';
import { graphql, QueryRenderer } from 'react-relay';
import { environment } from '../../environment';
import { LoadingError } from '../../components/nonideal/loadingerror';
import { Loading } from '../../components/nonideal/loading';
import Helmet from 'react-helmet';
import { CaseNavigator } from '../../components/caseNavigator';
import { Route, Switch } from 'react-router';
import { CaseDetail } from './detail';
import { CaseListContainer } from './list/CaseList';

export interface ScreenCaseHandlingProps {
    baseUrl: string;
    history: any,
}

export interface ScreenCaseHandlingState {
    cases: any[];
    caseActions: object;
    threadActions: object;
    observables: any[];
    observableThreads: any[];
}

const ScreenCaseHandlingQuery = graphql`
    query ScreenCaseHandlingQuery {
        viewer {
            ...CaseList_viewer
        }
    }
`;

export class ScreenCaseHandling extends React.Component<ScreenCaseHandlingProps, ScreenCaseHandlingState> {
    private keeks: string = 'tere';

    constructor (props: ScreenCaseHandlingProps) {
        super(props);
        this.state = {
            cases: [],
            caseActions: [],
            threadActions: [],
            observables: [],
            observableThreads: [],
        }
    }

    render () {
        const {baseUrl} = this.props;
        return (
            <QueryRenderer
                environment={environment}
                variables={{}}
                query={ScreenCaseHandlingQuery}
                render={({error, props}) => {
                    if (error) {
                        return (
                            <div className="layout-content">
                                <Helmet>
                                    <title>Cases loading failed : ComRoom{this.keeks}</title>
                                </Helmet>
                                <div className="page-content">
                                    <LoadingError message={error.message}/>
                                </div>
                            </div>
                        );
                    }
                    if (props) {
                        // todo :: refactor this and move stuff where it belongs
                        // const allCases = props.viewer.allCases.edges.map((node: any) => node.node);
                        return (
                            <div className="layout-content">
                                <CaseNavigator
                                    baseUrl={baseUrl}
                                    cases={[]}
                                    observables={this.state.observables}
                                    observableThreads={this.state.observableThreads}
                                    caseActions={this.state.caseActions}
                                />
                                <Switch>
                                    <Route path={baseUrl + '/:caseId'}>
                                        <CaseDetail
                                            baseUrl={baseUrl}
                                            allCases={[]}
                                            observableThreads={this.state.observableThreads}
                                            caseActions={this.state.caseActions}
                                            threadActions={this.state.threadActions}
                                        />
                                    </Route>
                                    <Route path={baseUrl}>
                                        <CaseListContainer
                                            baseUrl={this.props.baseUrl}
                                            caseActions={this.state.caseActions}
                                            viewer={props.viewer}
                                        />
                                    </Route>
                                </Switch>
                            </div>
                        );
                    }
                    return (
                        <div className="layout-content">
                            <Helmet>
                                <title>Loading Cases : ComRoom</title>
                            </Helmet>
                            <div className="page-content">
                                <Loading/>
                            </div>
                        </div>
                    );
                }}
            />
        );
    }
}

