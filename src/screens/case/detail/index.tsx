import * as React from 'react';
import * as classNames from 'classnames';

import Helmet from 'react-helmet';
import { Position, Tooltip } from '@blueprintjs/core';
import { Link, NavLink } from 'react-router-dom';
import { Icon } from '../../../components/ui/Icon';

export interface SubMenuProps {
    className: string;
    baseUrl: string;
}

const Submenu = (props: SubMenuProps) => {
    const {baseUrl, className} = props;
    return (
        <ul className={className}>
            <li className="main not-mobile-hide"><NavLink to={baseUrl} exact>Chat</NavLink></li>
            <li className="main not-mobile-hide"><NavLink to={baseUrl + '/details'} exact>Case details</NavLink></li>
            <li className="main mobile-hide"><NavLink to={baseUrl} exact>Case details</NavLink></li>
            <li className="main"><NavLink to={baseUrl + '/client-details'}>Client details</NavLink></li>
            <li className="main"><NavLink to={baseUrl + '/screen-sharing'}>Share screen</NavLink></li>
            <li className="main"><NavLink to={baseUrl + '/comments'}><span className="tag">3</span> Comments</NavLink></li>
            <li className="main"><NavLink to={baseUrl + '/log'}>Case log</NavLink></li>
        </ul>
    );
};

export interface CaseDetailProps {
    match?: any;
    baseUrl: string;
    caseActions: any;
    threadActions: any;
    allCases: any[];
    observableThreads: any[];
}
export interface CaseDetailState {
    caseExists: boolean;
    caseIsObservable: boolean;
    currentCase: any;
    currentThread: object;
    istyping?: any;
    menuOpen?: boolean;
}
export class CaseDetail extends React.Component<CaseDetailProps, CaseDetailState> {
    render() {
        let ret = null;
        if (this.state.currentCase && this.state.caseIsObservable) {
            ret = (
                <div className="page-content">
                    <Helmet>
                        <title>Case {this.state.currentCase.id} : ComRoom</title>
                    </Helmet>
                    <div className="container-case">
                        <div className={classNames('col-chat', { active: this.props.match.isExact })}>
                            <div className="inner">
                                {/* <ChatFilter/> */}
                                <div className="tools">
                                    <Tooltip content="End chat" position={Position.RIGHT}>
                                        <button onClick={() => { this.props.threadActions.sendMessage(this.state.currentCase.id, 'Chat was closed by Me.', 'system'); }}><Icon kind="close" wrapperClassName="icon"/></button>
                                    </Tooltip>

                                    <Tooltip content="Steve and John are observing" position={Position.RIGHT}>
                                        <button><Icon kind="eye" wrapperClassName="icon"/></button>
                                    </Tooltip>
                                </div>
                            </div>
                        </div>
                        <div className={classNames('col-menu', { open: this.state.menuOpen })}>
                            <div className="mobile-submenu" onClick={() => { this.setState({ menuOpen: !this.state.menuOpen }); }}>
                                <button><Icon kind="arrow-down" wrapperClassName="icon" /></button>
                                <Submenu className="fakemenu" baseUrl={this.props.match.url} />
                            </div>
                            <div className="inner" onClick={() => { this.setState({ menuOpen: false }); }}>
                                <Submenu className="menu" baseUrl={this.props.match.url} />
                            </div>
                        </div>
                        <div className={classNames('col-details', { active: !this.props.match.isExact })}>
                            <div className="inner">
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            ret = (
                <div className="page-content">
                    <Helmet>
                        <title>Case not found : ComRoom</title>
                    </Helmet>
                    <div className="pt-non-ideal-state pt-intent-danger">
                        <p className="illustration"><Icon kind="bug" wrapperClassName="icon" /></p>
                        <p className="heading">There is no such case</p>
                        <p className="description">Maybe you used an outdated link or do not have privileges to access this case.</p>
                        <p className="action">
                            <Link className="pt-button pt-intent-primary" to={'/cases'}>Continue</Link>
                        </p>
                    </div>
                </div>
            );
        }
        return ret;
    }
}
