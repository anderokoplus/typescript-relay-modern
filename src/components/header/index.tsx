import * as React from 'react';
import { Icon } from '../ui/Icon';
import { Link, NavLink } from 'react-router-dom';
import { Popover, Position } from '@blueprintjs/core';
import { RouteComponentProps, withRouter } from 'react-router';
import { clearUserData } from '../../constants';

export interface HeaderProps extends RouteComponentProps<{}> {
    history: any;
    currentUser: any;
}

export interface HeaderState {
}

class Header extends React.Component<HeaderProps, HeaderState> {
    logout = () => {
        clearUserData();
        this.props.history.push('/login');
    };

    render () {
        const currentUser = {
            firstname: 'foo',
            lastname: 'bar',
        };
        return (
            <div className="layout-header">
                <button className="menutoggle">
                    <Icon kind="menu" wrapperClassName="icon"/>
                </button>
                <NavLink className="logo" to="/">
                    <Icon kind="logo-horizontal"/>
                </NavLink>
                <div className="userhub">
                    <Popover position={Position.BOTTOM_RIGHT} portalClassName="userdrop">
                        <div className="userinfo available">
                            <div className="image" style={{ backgroundImage: 'url(assets/img/agent01.png)' }}/>
                            <div className="name">{currentUser.firstname + ' ' + currentUser.lastname}</div>
                            <div className="meta">Available 2 / 10</div>
                            <Icon kind="arrow-down" wrapperClassName="icon"/>
                        </div>
                        <div className="usermenu pt-popover-dismiss">
                            <p>My status:</p>
                            <ul className="menu">
                                <li><a href="#"><Icon kind="check" className="icon"/>Available</a></li>
                                <li><a href="#">Closing</a></li>
                                <li><a href="#">Offline</a></li>
                            </ul>
                            <p>My account</p>
                            <ul className="menu">
                                <li><Link to="accountsettings">Settings</Link></li>
                                <li><Link to="styleguide">Styleguide</Link></li>
                                <li className="separated"><a href="#" onClick={(e) => {
                                    e.preventDefault();
                                    this.logout();
                                }}>Sign out</a></li>
                            </ul>
                        </div>
                    </Popover>
                </div>
                <ul className="menu">
                    <li><NavLink to="/" exact>Dashboard</NavLink></li>
                    <li><NavLink to="/cases">Case handling</NavLink></li>
                    <li><NavLink to="/users/simple-list">Users[SL]</NavLink></li>
                    <li><NavLink to="/users/cursor-pagination">Users[CP]</NavLink></li>
                    <li><NavLink to="/users/offset-pagination">Users[OPG]</NavLink></li>
                </ul>
            </div>
        );
    }
}

export const HeaderWithRouter = withRouter(Header);
