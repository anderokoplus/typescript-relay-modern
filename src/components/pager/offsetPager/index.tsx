import * as React from 'react';
import { Button } from '@blueprintjs/core';
import { Icon } from '../../ui/Icon';

export interface OffsetPagerProps {
    limit: number;
    offset: number;
    total: number;
    onPreviousPageClick: () => void;
    onNextPageClick: () => void;
    onGoToPageClick: (pageNumber: number) => void;
}

export class OffsetPager extends React.Component<OffsetPagerProps, {}> {
    render () {
        const { limit, offset, total } = this.props;
        const numberOfPages = total ? Math.ceil(total / limit) : 0;
        if (!numberOfPages) {
            return null;
        }
        const currentPage = (offset / limit) + 1;
        return (
            <p className="pt-button-group pt-minimal pt-paginator">
                <Button
                    onClick={() => {
                        this.props.onPreviousPageClick()
                    }}
                    disabled={1 === currentPage}
                >
                    <Icon kind="arrow-left" wrapperClassName="icon rounded"/>Previous
                </Button>
                {(Array.apply(null, Array(numberOfPages))).map((v: any, pageNr: number) => {
                    return (
                        <Button
                            key={pageNr}
                            onClick={() => {
                                this.props.onGoToPageClick(pageNr + 1)
                            }}
                            className={pageNr + 1 === currentPage ? 'pt-active' : ''}
                        >
                            {pageNr + 1}
                        </Button>
                    );
                })}
                <Button
                    onClick={() => {
                        this.props.onNextPageClick()
                    }}
                    disabled={numberOfPages === currentPage}
                >
                    Next<Icon kind="arrow-right" wrapperClassName="icon rounded after"/>
                </Button>
            </p>
        );
    }
}
