import * as React from 'react';
import * as classNames from 'classnames';
import * as PropTypes from 'prop-types';

import { ITableContext } from './Table';
import { Icon } from '../Icon';
import { calculateHiddenMapping, calculateVisibleMapping } from './index';

export interface TableRowProps {
    isOpen?: boolean;
    children?: any;
    className?: string;
}

export interface TableRowState {
    isOpen: boolean;
}

export class TableRow extends React.Component<TableRowProps, TableRowState> {
    static contextTypes = {
        tableStore: PropTypes.object
    };
    context: ITableContext;

    constructor(props: TableRowProps) {
        super(props);
        this.state = {
            isOpen: props.isOpen || false
        };
    }

    public render() {
        const {headers, headersMapping, tableWidth} = this.context.tableStore;
        if (tableWidth === 0) { // not initialized, render full table
            const rows = this.props.children.map((child: any, index: number) => {
                return headers[index].props.hidden ? null :
                    <td key={index} className={classNames(child.props.className)}>{child}</td>;
            });
            return (
                <tbody>
                    <tr className={classNames(this.props.className)}>{rows}</tr>
                </tbody>
            );
        } else {

            const visibleHeaders = calculateVisibleMapping(headersMapping, tableWidth);
            const hiddenHeaders = calculateHiddenMapping(headersMapping, tableWidth);

            const mapper = (map: any) => {
                return {
                    header: map.header,
                    row: this.props.children[map.index]
                };
            };

            const visibleChildrenContainer = visibleHeaders.map(mapper);
            const hiddenChildrenContainer = hiddenHeaders.map(mapper);

            const toggle = () => {
                this.setState({isOpen: !this.state.isOpen});
            };

            const visibleFields = visibleChildrenContainer.map((container, index) => {
                if (index === 0 && hiddenChildrenContainer.length > 0) {
                    return (
                        <td key={index} className={classNames('toggler', container.row.props.className)}>
                            <button onClick={toggle} className="pt-button pt-small icon"><Icon kind={this.state.isOpen ? 'arrow-up' : 'arrow-down'} wrapperClassName="icon"/></button>
                            {container.row}
                        </td>
                    );
                }
                return (
                    <td key={index} className={container.row.props.className}>{container.row}</td>
                );
            });
            const hiddenFields = hiddenChildrenContainer.map((container, index) => {
                return (
                    <div key={index} className={classNames('row', container.row.props.className)}>
                        <div className="label"><b>{container.header}</b></div>
                        <div className="items">{container.row}</div>
                    </div>
                );
            });

            const rows = [<tr key="1" className={this.props.className}>{visibleFields}</tr>];
            if (hiddenFields.length > 0 && this.state.isOpen) {
                rows.push((
                    <tr key="2" className="sub">
                        <td colSpan={visibleFields.length}>
                            {hiddenFields}
                        </td>
                    </tr>
                ));
            }
            return (
                <tbody>{rows}</tbody>
            );
        }
    }
}
