import * as React from 'react';
import * as PropTypes from 'prop-types';
import * as classNames from 'classnames';

import { findDOMNode } from 'react-dom';

import { throttle, uniqueId } from 'lodash';
import { TableHeadCell } from './HeadCell';

export interface ITableContext {
    tableStore: ITableStore;
}

export interface ITableStore {
    tableWidth: number;
    headers: TableHeadCell[];
    headersMapping: ITableHeadCellMapping[];
}

export interface ITableHeadCellMapping {
    header: TableHeadCell;
    index: number;
    width: number;
    priority: number;
}

export interface ITableProps {
    className?: string;
    children: any;
}

export class Table extends React.Component<ITableProps, {}> {

    static childContextTypes = {
        tableStore: PropTypes.object
    };

    tableStore: ITableStore;

    refs: {
        table: any
    };

    constructor (props: ITableProps) {
        super(props);
        this.updateDimensions = throttle(this.updateDimensions.bind(this), 200);
    }

    getChildContext (): ITableContext {
        if (!this.tableStore) {
            const tableHead = this.props.children[0];
            const headers: TableHeadCell[] = tableHead.props.children;
            this.tableStore = {
                tableWidth: 0,
                headers,
                headersMapping: []
            };
        }
        return {
            tableStore: this.tableStore
        };
    }

    render () {
        return (
            <div className="responsivetable">
                <table className={classNames('default', this.props.className)} ref="table">
                    {this.props.children}
                </table>
            </div>
        );
    }

    public updateTableWidth () {
        const wrapper = findDOMNode(this) as HTMLElement;
        // Measure required width and save for later use
        this.tableStore.tableWidth = wrapper.getBoundingClientRect().width;
    }

    public updateHeadersMapping () {
        const { headers } = this.tableStore;
        const wrapper = findDOMNode(this) as HTMLElement;
        const table = findDOMNode(this.refs.table) as HTMLElement;
        const CUSTOM_PRIORITY_OFFSET = 10000;

        // Set wrapper in measuring mode, to see how wide columns and table really are
        wrapper.classList.add('measuring');

        // Map headers for measuring
        const mapping = headers.map((header, index) => {
            const priority = header.props.priority || (parseInt(uniqueId(), 10) + CUSTOM_PRIORITY_OFFSET);
            return {
                header,
                index,
                width: 0,
                priority
            };
        });

        // Measure only visible columns on correct index
        mapping.filter((n) => {
            return n.header.props.hidden !== true;
        }).map((header, index) => {
            mapping[header.index].width = table.children[0].children[0].children[index].getBoundingClientRect().width + (index === 0 ? 30 : 0);
        });

        // Step out of measuring mode
        wrapper.classList.remove('measuring');

        this.tableStore.headersMapping = mapping;
    }

    public updateDimensions () {
        this.updateTableWidth();
        this.forceUpdate();
    }

    public componentWillUnmount () {
        window.removeEventListener('resize', this.updateDimensions);
    }

    public componentDidMount () {
        window.addEventListener('resize', this.updateDimensions);
        this.updateTableWidth();
        this.updateHeadersMapping();
        this.forceUpdate();
    }
}
