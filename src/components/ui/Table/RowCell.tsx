import * as React from 'react';

export interface ITableRowCellProps {
    className?: string;
    visible?: boolean;
}

export class TableRowCell extends React.Component<ITableRowCellProps, {}> {
    public render() {
        return (
            <span>
                {this.props.children}
            </span>
        );
    }
}
