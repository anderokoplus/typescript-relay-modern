import * as React from 'react';

export interface ITableHeadCellProps {
    className?: string;
    hidden?: boolean;
    priority?: number;
}

export class TableHeadCell extends React.Component<ITableHeadCellProps, {}> {
    public render() {
        return (
            <span>
                {this.props.children}
            </span>
        );
    }
}
