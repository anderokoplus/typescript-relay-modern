import * as React from 'react';
import * as PropTypes from 'prop-types';

import { ITableContext } from './Table';
import { calculateVisibleMapping } from './index';

export interface TableRowHeaderProps {
    className?: string;
}

export interface TableRowHeaderState {

}

export class TableRowHeader extends React.Component<TableRowHeaderProps, TableRowHeaderState> {

    static contextTypes = {
        tableStore: PropTypes.object
    };

    context: ITableContext;

    constructor (props: TableRowHeaderProps) {
        super(props);
    }

    public render () {
        const { headersMapping, tableWidth } = this.context.tableStore;
        const colSpan = tableWidth === 0 ? headersMapping.length : calculateVisibleMapping(headersMapping, tableWidth).length;

        return (
            <tbody>
            <tr className={this.props.className}>
                <th colSpan={colSpan}>
                    {this.props.children}
                </th>
            </tr>
            </tbody>
        );
    }
}
