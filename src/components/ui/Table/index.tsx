import { ITableHeadCellMapping } from './Table';
export * from './Table';
export * from './Head';
export * from './HeadCell';
export * from './Row';
export * from './RowCell';
export * from './RowHeader';

import { first } from 'lodash';

const shouldHide = (map: ITableHeadCellMapping, remainingSpace: number) => {
    return remainingSpace < 0 || remainingSpace - map.width < 0;
};
const sortMappingByPriority = ((obj1: any, obj2: any) => obj1.priority - obj2.priority);
const sortMappingByIndex = ((obj1: any, obj2: any) => obj1.index - obj2.index);

export function calculateHiddenMapping (headersMapping: ITableHeadCellMapping[], remainingSpace: any): ITableHeadCellMapping[] {
    let tippingPoint = false; // Everything after the tipping point is hidden, to prevent field 8 (shorter) to appear after field 7 (longer) no longer fits
    return headersMapping
        .sort(sortMappingByPriority)
        .reduce(
            (original: any, map: any) => {
                if (tippingPoint) {
                    original.push(map);
                } else {
                    if (map.header.props.hidden) { // If the field is defined as hidden we simply add it
                        original.push(map);
                        return original;
                    }
                    const hidden = shouldHide(map, remainingSpace);
                    if (hidden) {
                        original.push(map);
                        tippingPoint = true;
                    } else {
                        remainingSpace -= map.width;
                    }
                }
                return original;
            },
            [])
        .sort(sortMappingByIndex);

}

export function calculateVisibleMapping (headersMapping: ITableHeadCellMapping[], remainingSpace: any): ITableHeadCellMapping[] {
    const hiddenMapping = calculateHiddenMapping(headersMapping, remainingSpace);
    const visible = headersMapping
        .sort(sortMappingByPriority)
        .reduce(
            (original: any, map: any) => {
                if (original.length === (headersMapping.length - hiddenMapping.length)) { // make sure we do not show more visible than we should (tipping point)
                    return original;
                }
                if (map.header.props.hidden) { // if its hidden, we simply ignore it
                    return original;
                }
                const hidden = shouldHide(map, remainingSpace);
                if (!hidden) {
                    original.push(map);
                    remainingSpace -= map.width;
                }
                return original;
            },
            []
        )
        .sort(sortMappingByIndex);

    if (visible.length === 0) { // Make sure we always show the first column, even if it does not fit
        const firstHeader = first(headersMapping.sort(sortMappingByPriority));
        if (firstHeader === undefined) {
            return [];
        }
        return [firstHeader];
    }
    return visible;
}
