import * as React from 'react';
import * as PropTypes from 'prop-types';

import { ITableContext } from './Table';
import { calculateVisibleMapping } from './index';

export interface ITableHeadProps {
    children?: any;
    className?: string;
}

export class TableHead extends React.Component<ITableHeadProps, {}> {

    static contextTypes = {
        tableStore: PropTypes.object
    };

    context: ITableContext;

    constructor(props: ITableHeadProps) {
        super(props);
    }

    public render() {
        const {tableWidth, headersMapping, headers} = this.context.tableStore;
        const mapper = ((header: any, index: number) => {
            return header.props.hidden ? null : <th key={index} className={header.props.className}>{header}</th>;
        });
        let headings;
        if (tableWidth === 0) { // not initialized
            headings = headers.map(mapper);
        } else {
            headings = calculateVisibleMapping(headersMapping, tableWidth)
                .map(map => map.header)
                .map(mapper);
        }
        return (
            <thead>
                <tr className={this.props.className}>{headings}</tr>
            </thead>
        );
    }
}
