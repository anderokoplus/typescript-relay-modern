import * as React from 'react';
import { saveUserData } from '../../constants';
import { login } from '../../mutations/LoginMutation';
import { RouteComponentProps, withRouter } from 'react-router';

export interface LoginFormProps extends RouteComponentProps<{}> {
    history: any;
}

export interface LoginFormState {
    username: string;
    password: string;
}

class LoginForm extends React.Component<LoginFormProps, LoginFormState> {

    constructor (props: LoginFormProps) {
        super(props);
        this.state = {
            username: '',
            password: '',
        };
    }

    login = () => {
        login(
            this.state.username,
            this.state.password,
            (response) => {
                if (response.errors) {
                    console.error(response.errors);
                    return;
                } else {
                    saveUserData(response.user.id, response.token);
                }
            },
            (error) => {
                console.error(error);
            }
        );
    };

    render () {
        return (
            <div>
                <div className="form horizontal">
                    <div className="row wide">
                        <label className="label">Username</label>
                        <div className="items">
                            <input
                                type="text"
                                className="pt-form-control"
                                onChange={(e) => {
                                    this.setState({ username: e.target.value });
                                }}
                            />
                        </div>
                    </div>
                    <div className="row wide">
                        <label className="label">Password</label>
                        <div className="items">
                            <input
                                type="password"
                                className="pt-form-control"
                                onChange={(e) => {
                                    this.setState({ password: e.target.value });
                                }}
                            />
                        </div>
                    </div>
                </div>
                <ul className="buttons">
                    <li className="primary">
                        <button className="pt-button pt-intent-primary" onClick={this.login}>Login</button>
                    </li>
                    <li className="secondary">
                        <button className="pt-button pt-minimal">I forgot my password</button>
                    </li>
                </ul>
            </div>
        );
    }
}

export const LoginFormWithRouter = withRouter(LoginForm);
