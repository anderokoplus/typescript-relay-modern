import * as React from 'react';
import { NavLink } from 'react-router-dom';

import { Icon } from '../ui/Icon';
import { CaseNavigatorScroller } from './caseNavigatorScroller';

export interface CaseNavigatorProps {
    baseUrl: string;
    cases: any[];
    observables: any[];
    observableThreads: any[];
    caseActions: any;
}

export interface CaseNavigatorState {
}

export class CaseNavigator extends React.Component<CaseNavigatorProps, CaseNavigatorState> {
    render () {
        const {baseUrl} = this.props;
        return (
            <div className="casenavigator">
                <NavLink className="allcases" to="/cases" exact>
                    <Icon kind="arrow2-left" wrapperClassName="icon"/>
                    <span className="label">
                        {this.props.cases.filter((a) => {
                            return a.status === 'open';
                        }).length} open cases
                    </span>
                    <span className="meta">
                        {this.props.cases.filter((item) => { return item.assignee === null; }).length} unassigned
                    </span>
                </NavLink>
                <div className="nav">
                    {
                        this.props.observables.length > 0
                            ? (
                                <CaseNavigatorScroller baseUrl={baseUrl} cases={this.props.cases}
                                                       observables={this.props.observables}
                                                       observableThreads={this.props.observableThreads}
                                                       caseActions={this.props.caseActions}/>
                            )
                            : (
                                <div className="msg">
                                    <Icon kind="info" wrapperClassName="icon"/>
                                    <span className="text">No cases to display.</span>
                                </div>
                            )
                    }
                </div>
            </div>
        );
    }
}
