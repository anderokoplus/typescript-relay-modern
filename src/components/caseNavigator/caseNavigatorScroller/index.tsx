import * as React from 'react';

export interface CaseNavigatorScrollerProps {
    baseUrl: string;
    cases: any[];
    observables: any[];
    observableThreads: any[];
    caseActions: any;
}

export interface CaseNavigatorScrollerState {
    isScrolling?: boolean;
    scrollLeft?: number;
    clientX?: number;
    animating?: boolean;
}

export class CaseNavigatorScroller extends React.Component<CaseNavigatorScrollerProps, CaseNavigatorScrollerState> {
    constructor(props: CaseNavigatorScrollerProps) {
        super(props);
        this.state = {
            isScrolling: false,
            clientX: 0,
            scrollLeft: 0,
            animating: false
        };
    }
    render () {
        return (
            <div className="scroller"/>
        );
    }
}

