import * as React from 'react';
import { Icon } from '../../ui/Icon';

export class Loading extends React.Component<{}, {}> {
    render() {
        return (
            <div className="pt-non-ideal-state animation-fade-in">
                <p className="illustration"><Icon kind="loading" wrapperClassName="icon" className="animation-spin" /></p>
                <p className="heading">Loading, please wait...</p>
            </div>
        );
    }
}
