import * as React from 'react';
import { Icon } from '../../ui/Icon';

export class LoadingError extends React.Component<{ message: string; }, {}> {
    render() {
        return (
            <div className="pt-non-ideal-state pt-intent-danger">
                <p className="illustration"><Icon kind="bug" wrapperClassName="icon" /></p>
                <p className="heading">Loading failed</p>
                {
                    this.props.message
                        ? <p className="heading">Loading failed {this.props.message}</p>
                        : <p className="description">Unable to access data required to display this page.</p>
                }
            </div>
        );
    }
}
