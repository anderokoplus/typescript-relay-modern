import { execute } from 'apollo-link';
import {
    CacheConfig,
    ConcreteBatch,
    Disposable,
    Environment,
    Network,
    QueryPayload,
    RecordSource,
    RelayObservable,
    Store,
    UploadableMap,
    Variables,
} from 'relay-runtime';
import { getAuthToken, GRAPHQL_ENDPOINT, GRAPHQL_SUBSCRIPTION_ENDPOINT } from './constants';
import { SubscriptionClient } from 'subscriptions-transport-ws';
import { WebSocketLink } from 'apollo-link-ws';


const fetchQuery = (operation: ConcreteBatch, variables: Variables, cacheConfig: CacheConfig, uploadables?: UploadableMap) => {
    return fetch(GRAPHQL_ENDPOINT, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': `Bearer ${getAuthToken()}`,
        },
        body: JSON.stringify({
            query: operation.text,
            variables,
        }),
    }).then(response => {
        return response.json();
    });
};

const subscriptionClient = new SubscriptionClient(GRAPHQL_SUBSCRIPTION_ENDPOINT, { reconnect: true });
const subscriptionLink = new WebSocketLink(subscriptionClient);
const networkSubscriptions = (operation: ConcreteBatch, variables: Variables): RelayObservable<QueryPayload> | Disposable => {
    const observable = execute(subscriptionLink, {
        query: operation.text,
        variables,
    });
    return Object.assign(observable, {
        dispose: () => {
            console.log('dispose called', {
                operation,
                variables,
                observable
            });
            debugger;
        }
    });
};

export const environment = new Environment({
    network: Network.create(fetchQuery, networkSubscriptions),
    store: new Store(new RecordSource()),

});

