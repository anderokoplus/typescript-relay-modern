import { graphql } from 'react-relay';
import { commitMutation } from 'react-relay/compat';
import { environment } from '../environment';
import { PayloadError } from 'relay-runtime';

const mutation = graphql`
    mutation LoginMutation($input: LoginMutationInput!) {
        loginMutation(input: $input) {
            user {
                id
                firstName
                lastName
                role
                image
            }
            token
            errors {
                code
                key
                message
            }
        }
    }
`;

// todo :: replace "response" object type any with exact type, also wrap this into factory
export const login = (username: string, password: string, onCompleted: (response: any, errors: PayloadError[] | null | undefined) => void, onError: (error?: Error) => void) => {
    const variables = {
        input: {
            username,
            password,
        }
    };
    commitMutation(
        environment,
        {
            mutation,
            variables,
            onCompleted: (response, errors) => {
                onCompleted(response.loginMutation, errors);
            },
            onError,
        }
    );
};
