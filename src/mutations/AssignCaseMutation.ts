import { graphql } from 'react-relay';
import { commitMutation } from 'react-relay/compat';
import { environment } from '../environment';
import { PayloadError } from 'relay-runtime';

const mutation = graphql`
    mutation AssignCaseMutation($input: AssignCaseMutationInput!) {
        assignCaseMutation(input: $input) {
            case {
                node {
                    id
                    assignee {
                        id
                        firstName
                        lastName
                        role
                        image
                    }
                }
            }
            errors {
                code
                key
                message
            }
        }
    }
`;

// todo :: replace "response" object type any with exact type, also wrap this into factory
export const assignCase = (caseId: string, userId: string, onCompleted: (response: any, errors: PayloadError[] | null | undefined) => void, onError: (error?: Error) => void) => {
    const variables = {
        input: {
            caseId,
            userId,
        }
    };
    commitMutation(
        environment,
        {
            mutation,
            variables,
            // optimisticUpdater: (proxyStore) => {
            //     const caseItem = proxyStore.get(caseId);
            // },
            // updater: (proxyStore) => {
            //     const caseItem = proxyStore.get(caseId);
            // },
            onCompleted,
            onError,
        }
    );
};
