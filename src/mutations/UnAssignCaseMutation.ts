import { graphql } from 'react-relay';
import { commitMutation } from 'react-relay/compat';
import { environment } from '../environment';
import { PayloadError } from 'relay-runtime';

const mutation = graphql`
    mutation UnAssignCaseMutation($input: UnAssignCaseMutationInput!) {
        unAssignCaseMutation(input: $input) {
            case {
                node {
                    id
                    assignee {
                        id
                        firstName
                        lastName
                        role
                        image
                    }
                }
            }
            errors {
                code
                key
                message
            }
        }
    }
`;

// todo :: replace "response" object type any with exact type, also wrap this into factory
export const unAssignCase = (caseId: string, onCompleted: (response: any, errors: PayloadError[] | null | undefined) => void, onError: (error?: Error) => void) => {
    const variables = {
        input: {
            caseId,
        }
    };
    commitMutation(
        environment,
        {
            mutation,
            variables,
            onCompleted,
            onError,
        }
    );
};
